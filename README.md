# Web Scraping Scirate

Scirate is a famous site in the quantum information scientific community. 
It is like ArXiv, but everyone can Scite a paper. Here we are doing simple webscraping
to create a dataset out of the data on the website. We make use of Beautifulsoup module
to parse html and then pandas library to convert the data into a csv file.